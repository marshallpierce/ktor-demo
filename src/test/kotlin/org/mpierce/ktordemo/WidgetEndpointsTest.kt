package org.mpierce.ktordemo

import com.fasterxml.jackson.databind.node.ArrayNode
import io.ktor.client.request.get
import io.ktor.client.request.post
import io.ktor.client.request.setBody
import io.ktor.client.statement.bodyAsText
import io.ktor.http.ContentType
import io.ktor.http.HttpStatusCode
import io.ktor.http.contentType
import io.ktor.server.testing.testApplication
import org.jooq.DSLContext
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

internal class WidgetEndpointsTest {
    /**
     * A contrived test that shows how to *not* use our custom test helper.
     */
    @Test
    internal fun getBadWidgetIdGets404() =
        testApplication {
            application {
                testAppSetup()
            }
            val response = client.get("/widgets/id/${Integer.MAX_VALUE}")
            assertEquals(HttpStatusCode.NotFound, response.status)
        }

    /**
     * Another contrived test, this time showing how to use the injector.
     */
    @Test
    internal fun getWidgetReturnsWidget() =
        testApplication {
            val widget =
                appWithInjector { injector ->
                    val jooq = injector.getInstance(DSLContext::class.java)
                    val daoFactory = injector.getInstance(DaoFactory::class.java)

                    // transaction isn't needed for in-memory dao impl, but you could run this test with SQL and it would work!
                    val widget =
                        jooq.transactionResult { c ->
                            daoFactory.widgetDao(c.dsl()).createWidget("foo")
                        }

                    widget
                }

            val response = client.get("/widgets/id/${widget.id}")

            assertEquals(HttpStatusCode.OK, response.status)

            // compare the json in a way that ignores whitespace, etc
            DEFAULT_JSON_TEST_HELPER.assertJsonStrEquals(
                """{
                        "id": ${widget.id},
                        "name": "${widget.name}",
                        "createdAt": "${widget.createdAt}"
                    }""",
                response.bodyAsText(),
            )
        }

    @Test
    internal fun createWidgetEndpointCreatesWidget() =
        testApplication {
            val (jooq, daoFactory) =
                appWithInjector { injector ->
                    Pair(injector.getInstance(DSLContext::class.java), injector.getInstance(DaoFactory::class.java))
                }

            val response =
                client.post("/widgets") {
                    contentType(ContentType.Application.Json)
                    setBody(
                        """
                        {
                            "name": "qwerty"
                        }""",
                    )
                }

            assertEquals(HttpStatusCode.OK, response.status)

            val rootNode = DEFAULT_JSON_TEST_HELPER.reader.readTree(response.bodyAsText())
            val id = rootNode.at("/id").intValue()

            val widget =
                jooq.transactionResult { c ->
                    daoFactory.widgetDao(c.dsl()).getWidget(id)
                }

            assertNotNull(widget)
            assertEquals("qwerty", widget.name)
        }

    @Test
    internal fun getAllWidgetsGetsThemAll() =
        testApplication {
            val widgets =
                appWithInjector { injector ->
                    val jooq = injector.getInstance(DSLContext::class.java)
                    val daoFactory = injector.getInstance(DaoFactory::class.java)

                    jooq.transactionResult { c ->
                        listOf(
                            daoFactory.widgetDao(c.dsl()).createWidget("foo"),
                            daoFactory.widgetDao(c.dsl()).createWidget("bar"),
                            daoFactory.widgetDao(c.dsl()).createWidget("baz"),
                        )
                    }
                }

            val response = client.get("/widgets/all")

            assertEquals(HttpStatusCode.OK, response.status)

            val rootNode = DEFAULT_JSON_TEST_HELPER.reader.readTree(response.bodyAsText()) as ArrayNode

            assertEquals(widgets.size, rootNode.size())

            widgets.zip(rootNode).forEach { (widget, node) ->
                assertEquals(widget.id, node.at("/id").intValue())
                assertEquals(widget.name, node.at("/name").textValue())
            }
        }
}
